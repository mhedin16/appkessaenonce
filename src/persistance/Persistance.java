
package persistance;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;


public class Persistance {
    
    private static EntityManager em;
    
   
    public static EntityManager getEm(){
    
         if (em==null)em= Persistence.createEntityManagerFactory("PU").createEntityManager();
    
         return em;
    }
}
