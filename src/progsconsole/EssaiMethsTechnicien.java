/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package progsconsole;

import entites.Technicien;
import java.util.Date;
    import java.text.SimpleDateFormat;
    import java.util.Calendar;
    import java.util.Date;

/**
 *
 * @author Administrateur
 */
public class EssaiMethsTechnicien {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Technicien tech = Technicien.getLeTechnicienNumero(801L);
        System.out.println("\nTest de la méthode coût horaire\n");
        System.out.println("Technicien N°: "+tech.getNumero());
        System.out.println("Nom: "+tech.getNom());
        System.out.println("Grade: "+tech.getLeGrade().getLibelle());
        System.out.println("Taux horaire afférent au grade: "+tech.getLeGrade().getTauxHoraire()+"€");
        System.out.println("Embauché(e) le: "+utilitaires.UtilDate.format(tech.getDateEmbauche()));
        System.out.println("Ancienneté: "+tech.getAnciennete()+" ans");
        System.out.println("Coeff majoration: "+tech.getCoeffMajo());
        
        
        System.out.println("\nCout Horaire: "+tech.coutHoraireTechnicien()+"€\n");
    }
  
}
